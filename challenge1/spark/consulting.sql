select *
from flights f 


-- 2. SQL transformation of flight, and sched_dep_time.
-- ● sched_dep_time: Scheduled departure time following the format HH:MM, ex. 05:15.

select string_to_array(sched_dep_time,'')(0)
from flights f ;


select
SUBSTRING(sched_dep_time,-2 , -1)
from flights f ;