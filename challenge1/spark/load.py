from pyspark.sql import SparkSession

spark = SparkSession.builder \
    .appName("BEON") \
    .config("spark.jars", "/home/gabriel.silva/Documents/beon/postgresql-42.6.2.jar") \
    .getOrCreate()

# load the csv
flight_df = spark.read\
    .option("delimiter", ",")\
    .option("header", True)\
    .csv("/home/gabriel.silva/Documents/beon/data-engineer-coding-interview/challenge1/dataset/nyc_flights.csv")

# (origin_name, final_name)
mapping_name = [
    ("dep_time", "actual_dep_time"),
    ("arr_time", "actual_arr_time")
]

# drop the first column
flight_df = flight_df.drop("_c0")

# check if the names (mapping) is the same
for origin_name, final_name in mapping_name:
    flight_df = flight_df\
        .withColumnRenamed(origin_name, final_name)

# type convert
mapping_type = 

("flight", "int")
("year", "int")
("month", "int")
("day", "int")
("hour", "int")
("minute", "int")
("actual_dep_time", "int")
("sched_dep_time", "int")
("dep_delay", "int")
("actual_arr_time", "int")
("sched_arr_time", "int")
("arr_delay", "int")


# load data
pg_database = "dw_flights"
pg_host = "localhost"
pg_password = "Pass1234"
pg_user = "postgres"
table = "flights"

properties = {
    "driver": "org.postgresql.Driver",
    "user": pg_user,
    "password": pg_password,
}
url = f"jdbc:postgresql://{pg_host}:5432/{pg_database}"
flight_df.write.jdbc(url=url, table=table, mode="Append", properties=properties)

